﻿namespace WinRest
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnCocacola = new System.Windows.Forms.Button();
            this.btnSprite = new System.Windows.Forms.Button();
            this.btnFanta = new System.Windows.Forms.Button();
            this.btnGuarana = new System.Windows.Forms.Button();
            this.btnIcetea = new System.Windows.Forms.Button();
            this.btnSagres = new System.Windows.Forms.Button();
            this.btnSuperbock = new System.Windows.Forms.Button();
            this.btnRedbull = new System.Windows.Forms.Button();
            this.btnMonster = new System.Windows.Forms.Button();
            this.btnSmirnoff = new System.Windows.Forms.Button();
            this.btnVodka = new System.Windows.Forms.Button();
            this.btnBaylis = new System.Windows.Forms.Button();
            this.btnBeirao = new System.Windows.Forms.Button();
            this.btnBacardi = new System.Windows.Forms.Button();
            this.btnMartini = new System.Windows.Forms.Button();
            this.btnCafe = new System.Windows.Forms.Button();
            this.lblAux = new System.Windows.Forms.Label();
            this.lblProgressoPorcentagem = new System.Windows.Forms.Label();
            this.trackBarContem = new System.Windows.Forms.TrackBar();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnServir = new System.Windows.Forms.Button();
            this.lblEscolhaCopo = new System.Windows.Forms.Label();
            this.cboCapacidadeCopo = new System.Windows.Forms.ComboBox();
            this.lblEscolhaBebida = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarContem)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCocacola
            // 
            this.btnCocacola.Image = ((System.Drawing.Image)(resources.GetObject("btnCocacola.Image")));
            this.btnCocacola.Location = new System.Drawing.Point(1, 4);
            this.btnCocacola.Name = "btnCocacola";
            this.btnCocacola.Size = new System.Drawing.Size(98, 57);
            this.btnCocacola.TabIndex = 0;
            this.btnCocacola.UseVisualStyleBackColor = true;
            this.btnCocacola.Click += new System.EventHandler(this.btnCocacola_Click);
            // 
            // btnSprite
            // 
            this.btnSprite.Image = ((System.Drawing.Image)(resources.GetObject("btnSprite.Image")));
            this.btnSprite.Location = new System.Drawing.Point(101, 4);
            this.btnSprite.Name = "btnSprite";
            this.btnSprite.Size = new System.Drawing.Size(98, 57);
            this.btnSprite.TabIndex = 1;
            this.btnSprite.UseVisualStyleBackColor = true;
            this.btnSprite.Click += new System.EventHandler(this.btnSprite_Click);
            // 
            // btnFanta
            // 
            this.btnFanta.Image = ((System.Drawing.Image)(resources.GetObject("btnFanta.Image")));
            this.btnFanta.Location = new System.Drawing.Point(200, 4);
            this.btnFanta.Name = "btnFanta";
            this.btnFanta.Size = new System.Drawing.Size(98, 57);
            this.btnFanta.TabIndex = 2;
            this.btnFanta.UseVisualStyleBackColor = true;
            this.btnFanta.Click += new System.EventHandler(this.btnFanta_Click);
            // 
            // btnGuarana
            // 
            this.btnGuarana.Image = ((System.Drawing.Image)(resources.GetObject("btnGuarana.Image")));
            this.btnGuarana.Location = new System.Drawing.Point(299, 4);
            this.btnGuarana.Name = "btnGuarana";
            this.btnGuarana.Size = new System.Drawing.Size(98, 57);
            this.btnGuarana.TabIndex = 3;
            this.btnGuarana.UseVisualStyleBackColor = true;
            this.btnGuarana.Click += new System.EventHandler(this.btnGuarana_Click);
            // 
            // btnIcetea
            // 
            this.btnIcetea.Image = ((System.Drawing.Image)(resources.GetObject("btnIcetea.Image")));
            this.btnIcetea.Location = new System.Drawing.Point(1, 62);
            this.btnIcetea.Name = "btnIcetea";
            this.btnIcetea.Size = new System.Drawing.Size(98, 57);
            this.btnIcetea.TabIndex = 4;
            this.btnIcetea.UseVisualStyleBackColor = true;
            this.btnIcetea.Click += new System.EventHandler(this.btnIcetea_Click);
            // 
            // btnSagres
            // 
            this.btnSagres.Image = ((System.Drawing.Image)(resources.GetObject("btnSagres.Image")));
            this.btnSagres.Location = new System.Drawing.Point(101, 62);
            this.btnSagres.Name = "btnSagres";
            this.btnSagres.Size = new System.Drawing.Size(98, 57);
            this.btnSagres.TabIndex = 5;
            this.btnSagres.UseVisualStyleBackColor = true;
            this.btnSagres.Click += new System.EventHandler(this.btnSagres_Click);
            // 
            // btnSuperbock
            // 
            this.btnSuperbock.Image = ((System.Drawing.Image)(resources.GetObject("btnSuperbock.Image")));
            this.btnSuperbock.Location = new System.Drawing.Point(200, 62);
            this.btnSuperbock.Name = "btnSuperbock";
            this.btnSuperbock.Size = new System.Drawing.Size(98, 57);
            this.btnSuperbock.TabIndex = 6;
            this.btnSuperbock.UseVisualStyleBackColor = true;
            this.btnSuperbock.Click += new System.EventHandler(this.btnSuperbock_Click);
            // 
            // btnRedbull
            // 
            this.btnRedbull.Image = ((System.Drawing.Image)(resources.GetObject("btnRedbull.Image")));
            this.btnRedbull.Location = new System.Drawing.Point(299, 62);
            this.btnRedbull.Name = "btnRedbull";
            this.btnRedbull.Size = new System.Drawing.Size(98, 57);
            this.btnRedbull.TabIndex = 7;
            this.btnRedbull.UseVisualStyleBackColor = true;
            this.btnRedbull.Click += new System.EventHandler(this.btnRedbull_Click);
            // 
            // btnMonster
            // 
            this.btnMonster.Image = ((System.Drawing.Image)(resources.GetObject("btnMonster.Image")));
            this.btnMonster.Location = new System.Drawing.Point(1, 121);
            this.btnMonster.Name = "btnMonster";
            this.btnMonster.Size = new System.Drawing.Size(98, 57);
            this.btnMonster.TabIndex = 8;
            this.btnMonster.UseVisualStyleBackColor = true;
            this.btnMonster.Click += new System.EventHandler(this.btnMonster_Click);
            // 
            // btnSmirnoff
            // 
            this.btnSmirnoff.Image = ((System.Drawing.Image)(resources.GetObject("btnSmirnoff.Image")));
            this.btnSmirnoff.Location = new System.Drawing.Point(101, 121);
            this.btnSmirnoff.Name = "btnSmirnoff";
            this.btnSmirnoff.Size = new System.Drawing.Size(98, 57);
            this.btnSmirnoff.TabIndex = 9;
            this.btnSmirnoff.UseVisualStyleBackColor = true;
            this.btnSmirnoff.Click += new System.EventHandler(this.btnSmirnoff_Click);
            // 
            // btnVodka
            // 
            this.btnVodka.Image = ((System.Drawing.Image)(resources.GetObject("btnVodka.Image")));
            this.btnVodka.Location = new System.Drawing.Point(200, 121);
            this.btnVodka.Name = "btnVodka";
            this.btnVodka.Size = new System.Drawing.Size(98, 57);
            this.btnVodka.TabIndex = 10;
            this.btnVodka.UseVisualStyleBackColor = true;
            this.btnVodka.Click += new System.EventHandler(this.btnVodka_Click);
            // 
            // btnBaylis
            // 
            this.btnBaylis.Image = ((System.Drawing.Image)(resources.GetObject("btnBaylis.Image")));
            this.btnBaylis.Location = new System.Drawing.Point(299, 121);
            this.btnBaylis.Name = "btnBaylis";
            this.btnBaylis.Size = new System.Drawing.Size(98, 57);
            this.btnBaylis.TabIndex = 11;
            this.btnBaylis.UseVisualStyleBackColor = true;
            this.btnBaylis.Click += new System.EventHandler(this.btnBaylis_Click);
            // 
            // btnBeirao
            // 
            this.btnBeirao.Image = ((System.Drawing.Image)(resources.GetObject("btnBeirao.Image")));
            this.btnBeirao.Location = new System.Drawing.Point(1, 178);
            this.btnBeirao.Name = "btnBeirao";
            this.btnBeirao.Size = new System.Drawing.Size(98, 57);
            this.btnBeirao.TabIndex = 12;
            this.btnBeirao.UseVisualStyleBackColor = true;
            this.btnBeirao.Click += new System.EventHandler(this.btnBeirao_Click);
            // 
            // btnBacardi
            // 
            this.btnBacardi.Image = ((System.Drawing.Image)(resources.GetObject("btnBacardi.Image")));
            this.btnBacardi.Location = new System.Drawing.Point(101, 178);
            this.btnBacardi.Name = "btnBacardi";
            this.btnBacardi.Size = new System.Drawing.Size(98, 57);
            this.btnBacardi.TabIndex = 13;
            this.btnBacardi.UseVisualStyleBackColor = true;
            this.btnBacardi.Click += new System.EventHandler(this.btnBacardi_Click);
            // 
            // btnMartini
            // 
            this.btnMartini.Image = ((System.Drawing.Image)(resources.GetObject("btnMartini.Image")));
            this.btnMartini.Location = new System.Drawing.Point(200, 178);
            this.btnMartini.Name = "btnMartini";
            this.btnMartini.Size = new System.Drawing.Size(98, 57);
            this.btnMartini.TabIndex = 14;
            this.btnMartini.UseVisualStyleBackColor = true;
            this.btnMartini.Click += new System.EventHandler(this.btnMartini_Click);
            // 
            // btnCafe
            // 
            this.btnCafe.Image = ((System.Drawing.Image)(resources.GetObject("btnCafe.Image")));
            this.btnCafe.Location = new System.Drawing.Point(299, 178);
            this.btnCafe.Name = "btnCafe";
            this.btnCafe.Size = new System.Drawing.Size(98, 57);
            this.btnCafe.TabIndex = 15;
            this.btnCafe.UseVisualStyleBackColor = true;
            this.btnCafe.Click += new System.EventHandler(this.btnCafe_Click);
            // 
            // lblAux
            // 
            this.lblAux.AutoSize = true;
            this.lblAux.Font = new System.Drawing.Font("HoloLens MDL2 Assets", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAux.Location = new System.Drawing.Point(34, 342);
            this.lblAux.Name = "lblAux";
            this.lblAux.Size = new System.Drawing.Size(11, 15);
            this.lblAux.TabIndex = 23;
            this.lblAux.Text = "-";
            // 
            // lblProgressoPorcentagem
            // 
            this.lblProgressoPorcentagem.AutoSize = true;
            this.lblProgressoPorcentagem.Font = new System.Drawing.Font("HoloLens MDL2 Assets", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgressoPorcentagem.Location = new System.Drawing.Point(307, 292);
            this.lblProgressoPorcentagem.Name = "lblProgressoPorcentagem";
            this.lblProgressoPorcentagem.Size = new System.Drawing.Size(16, 21);
            this.lblProgressoPorcentagem.TabIndex = 5;
            this.lblProgressoPorcentagem.Text = "-";
            // 
            // trackBarContem
            // 
            this.trackBarContem.Enabled = false;
            this.trackBarContem.Location = new System.Drawing.Point(2, 268);
            this.trackBarContem.Maximum = 100;
            this.trackBarContem.Name = "trackBarContem";
            this.trackBarContem.Size = new System.Drawing.Size(227, 45);
            this.trackBarContem.TabIndex = 17;
            this.trackBarContem.Scroll += new System.EventHandler(this.trackBarContem_Scroll);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(237, 268);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(156, 21);
            this.progressBar1.TabIndex = 4;
            // 
            // btnServir
            // 
            this.btnServir.Enabled = false;
            this.btnServir.Location = new System.Drawing.Point(101, 315);
            this.btnServir.Name = "btnServir";
            this.btnServir.Size = new System.Drawing.Size(197, 27);
            this.btnServir.TabIndex = 3;
            this.btnServir.Text = "PREPARAR BEBIDA";
            this.btnServir.UseVisualStyleBackColor = true;
            this.btnServir.Click += new System.EventHandler(this.btnServir_Click_1);
            // 
            // lblEscolhaCopo
            // 
            this.lblEscolhaCopo.AutoSize = true;
            this.lblEscolhaCopo.Font = new System.Drawing.Font("HoloLens MDL2 Assets", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEscolhaCopo.Location = new System.Drawing.Point(57, 239);
            this.lblEscolhaCopo.Name = "lblEscolhaCopo";
            this.lblEscolhaCopo.Size = new System.Drawing.Size(13, 16);
            this.lblEscolhaCopo.TabIndex = 2;
            this.lblEscolhaCopo.Text = "-";
            // 
            // cboCapacidadeCopo
            // 
            this.cboCapacidadeCopo.Enabled = false;
            this.cboCapacidadeCopo.FormattingEnabled = true;
            this.cboCapacidadeCopo.Location = new System.Drawing.Point(2, 237);
            this.cboCapacidadeCopo.Name = "cboCapacidadeCopo";
            this.cboCapacidadeCopo.Size = new System.Drawing.Size(53, 21);
            this.cboCapacidadeCopo.TabIndex = 1;
            this.cboCapacidadeCopo.Text = "Copo";
            this.cboCapacidadeCopo.SelectedIndexChanged += new System.EventHandler(this.cboCapacidadeCopo_SelectedIndexChanged);
            // 
            // lblEscolhaBebida
            // 
            this.lblEscolhaBebida.AutoSize = true;
            this.lblEscolhaBebida.Font = new System.Drawing.Font("HoloLens MDL2 Assets", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEscolhaBebida.Location = new System.Drawing.Point(213, 238);
            this.lblEscolhaBebida.Name = "lblEscolhaBebida";
            this.lblEscolhaBebida.Size = new System.Drawing.Size(13, 16);
            this.lblEscolhaBebida.TabIndex = 0;
            this.lblEscolhaBebida.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 362);
            this.Controls.Add(this.lblProgressoPorcentagem);
            this.Controls.Add(this.lblAux);
            this.Controls.Add(this.lblEscolhaCopo);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblEscolhaBebida);
            this.Controls.Add(this.btnServir);
            this.Controls.Add(this.btnCafe);
            this.Controls.Add(this.btnMartini);
            this.Controls.Add(this.btnBacardi);
            this.Controls.Add(this.cboCapacidadeCopo);
            this.Controls.Add(this.btnBeirao);
            this.Controls.Add(this.btnBaylis);
            this.Controls.Add(this.btnVodka);
            this.Controls.Add(this.btnSmirnoff);
            this.Controls.Add(this.btnMonster);
            this.Controls.Add(this.btnRedbull);
            this.Controls.Add(this.btnSuperbock);
            this.Controls.Add(this.btnSagres);
            this.Controls.Add(this.btnIcetea);
            this.Controls.Add(this.btnGuarana);
            this.Controls.Add(this.btnFanta);
            this.Controls.Add(this.btnSprite);
            this.Controls.Add(this.btnCocacola);
            this.Controls.Add(this.trackBarContem);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarContem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCocacola;
        private System.Windows.Forms.Button btnSprite;
        private System.Windows.Forms.Button btnFanta;
        private System.Windows.Forms.Button btnGuarana;
        private System.Windows.Forms.Button btnIcetea;
        private System.Windows.Forms.Button btnSagres;
        private System.Windows.Forms.Button btnSuperbock;
        private System.Windows.Forms.Button btnRedbull;
        private System.Windows.Forms.Button btnMonster;
        private System.Windows.Forms.Button btnSmirnoff;
        private System.Windows.Forms.Button btnVodka;
        private System.Windows.Forms.Button btnBaylis;
        private System.Windows.Forms.Button btnBeirao;
        private System.Windows.Forms.Button btnBacardi;
        private System.Windows.Forms.Button btnMartini;
        private System.Windows.Forms.Button btnCafe;
        private System.Windows.Forms.Label lblEscolhaBebida;
        private System.Windows.Forms.ComboBox cboCapacidadeCopo;
        private System.Windows.Forms.Label lblEscolhaCopo;
        private System.Windows.Forms.Button btnServir;
        private System.Windows.Forms.TrackBar trackBarContem;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lblProgressoPorcentagem;
        private System.Windows.Forms.Label lblAux;
    }
}


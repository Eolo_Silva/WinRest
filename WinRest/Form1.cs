﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinRest
{
    public partial class Form1 : Form
    {
        Copo meuCopo = new Copo();
        public Form1()
        {
            InitializeComponent();
            Copo meuCopo = new Copo();
        }
        private void btnCocacola_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Cocacola SELECT";
            meuCopo.Liquido = "Cocacola";
        }
        private void btnSprite_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Sprite SELECT";
            meuCopo.Liquido = "Sprite";
        }
        private void btnFanta_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Fanta SELECT";
            meuCopo.Liquido = "Fanta";
        }
        private void btnGuarana_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Guarana SELECT";
            meuCopo.Liquido = "Guarana";
        }
        private void btnIcetea_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "IceTea SELECT";
            meuCopo.Liquido = "IceTea";
        }
        private void btnSagres_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Sagres SELECT";
            meuCopo.Liquido = "Sagres";
        }
        private void btnSuperbock_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "SuperBock SELECT";
            meuCopo.Liquido = "SuperBock";
        }
        private void btnRedbull_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "RedBull SELECT";
            meuCopo.Liquido = "RedBull";
        }
        private void btnMonster_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Energy Monster SELECT";
            meuCopo.Liquido = "Monster";
        }
        private void btnSmirnoff_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Smirnoff SELECT";
            meuCopo.Liquido = "Smirnoff";
        }
        private void btnVodka_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Vodka SELECT";
            meuCopo.Liquido = "Vodka";
        }
        private void btnBaylis_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Baileys SELECT";
            meuCopo.Liquido = "Baileys";
        }
        private void btnBeirao_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Licor Beirão SELECT";
            meuCopo.Liquido = "Beirao";
        }
        private void btnBacardi_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Rum Bacardi SELECT";
            meuCopo.Liquido = "Bacardi";
        }
        private void btnMartini_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Aperitivo Martini SELECT";
            meuCopo.Liquido = "Martini";
        }
        private void btnCafe_Click(object sender, EventArgs e)
        {
            cboCapacidadeCopo.Enabled = true;
            lblEscolhaBebida.Text = "Café SELECT";
            meuCopo.Liquido = "Cafe";
        }
        private void cboCapacidadeCopo_SelectedIndexChanged(object sender, EventArgs e)
        {
            trackBarContem.Enabled = true;
            lblEscolhaCopo.Text = " Copo " + Convert.ToDouble(cboCapacidadeCopo.SelectedItem.ToString().Substring(0, 3)) + " ml SELECT";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //Adicionar Items à combo box capacidade
            List<string> capacidades = new List<string>
            {
                "500 ml",
                "330 ml",
                "250 ml",
                "200 ml"
            };
            foreach (var capacidade in capacidades)
            {
                cboCapacidadeCopo.Items.Add(capacidade);
            }
        }
        private void trackBarContem_Scroll(object sender, EventArgs e)
        {
            btnServir.Enabled = true;
            double conteudo = 0.0d;
            conteudo = meuCopo.Capacidade * trackBarContem.Value / 100;
            meuCopo.Contem = conteudo;
            meuCopo.ValorEmPercentagem();
            progressBar1.Value = meuCopo.ValorEmPercentagem();
            lblProgressoPorcentagem.Text = meuCopo.ValorEmPercentagem().ToString() + "%";
        }
        private void btnServir_Click_1(object sender, EventArgs e)
        {
            meuCopo = new Copo(meuCopo.Liquido, meuCopo.Capacidade);
            meuCopo.Encher(meuCopo.Capacidade);
            meuCopo.Capacidade = Convert.ToDouble(cboCapacidadeCopo.SelectedItem.ToString().Substring(0, 3));
            meuCopo.Contem = trackBarContem.Value;
            double servir = meuCopo.Contem;
            lblAux.Text = meuCopo.ToString();
        }
    }
}
